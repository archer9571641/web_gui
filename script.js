document.getElementById("rgbToHexButton").addEventListener("click", () => {
  const rgbInput = document.getElementById("rgbInput").value;
  const hex = rgbToHex(rgbInput);
  document.getElementById("output").innerText = `Hex: ${hex}`;
});

document.getElementById("hexToRgbButton").addEventListener("click", () => {
  const hexInput = document.getElementById("hexInput").value;
  const rgb = hexToRgb(hexInput);
  document.getElementById("output").innerText = `RGB: ${rgb}`;
});

function rgbToHex(rgb) {
  const rgbValues = rgb.split(",").map((value) => parseInt(value.trim()));

  if (rgbValues.length !== 3 || rgbValues.some(isNaN)) {
    return "Invalid input";
  }

  const hex = rgbValues
    .map((value) => {
      const hexValue = value.toString(16);
      return hexValue.length === 1 ? "0" + hexValue : hexValue;
    })
    .join("");

  return `#${hex.toUpperCase()}`;
}

function hexToRgb(hex) {
  hex = hex.replace(/^#/, "");

  if (!/^(?:[0-9a-fA-F]{3}){1,2}$/.test(hex)) {
    return "Invalid input";
  }

  if (hex.length === 3) {
    hex = hex
      .split("")
      .map((char) => char + char)
      .join("");
  }

  const red = parseInt(hex.substring(0, 2), 16);
  const green = parseInt(hex.substring(2, 4), 16);
  const blue = parseInt(hex.substring(4, 6), 16);

  return `${red}, ${green}, ${blue}`;
}
